#!/usr/bin/env python

import itertools
import numpy as np
from tiles import *

class TrivialBasis(object):
    """Uses the features themselves as a basis. However, does a little bit of basic manipulation
    to make things more reasonable. Specifically, this allows (defaults to) rescaling to be in the
    range [-1, +1].
    """

    def __init__(self, nvars, ranges):
        self.numTerms = nvars
        self.ranges = np.array(ranges)

    def scale(self, value, pos):
        if self.ranges[pos,0] == self.ranges[pos,1]:
            return 0.0
        else:
            return (value - self.ranges[pos,0]) / (self.ranges[pos,1] - self.ranges[pos,0])

    def getNumBasisFunctions(self):
        return self.numTerms

    def computeFeatures(self, features):
        if len(features) == 0:
            return np.ones((1,))
        return (np.array([self.scale(features[i],i) for i in range(len(features))]) - 0.5)*2.


class RBFBasis(TrivialBasis):
    """Radial Basis Functions basis. This implementation is just about as simplistic as it gets.
    This really could use some work to make it more competitive with state of the art.
    """

    def __init__(self, nvars, ranges, num_functions=10, beta=0.9):
        TrivialBasis.__init__(self, nvars, ranges)
        self.beta = beta
        self.num_functions = num_functions
        self.centers = np.random.uniform(self.ranges[:,0], self.ranges[:,1].T, (self.num_functions,self.numTerms))

    def getNumBasisFunctions(self):
        return self.num_functions

    def computeFeatures(self, features):
        if len(features) == 0:
            return np.ones((1,))
        features = np.array(features)
        return np.array([np.exp(-self.beta * np.linalg.norm(features-c)**2) for c in self.centers])


class RichTileCoderGroup(object):
    # TODO: rather than creating a new array we could pass in a buffer to write to
    # this should be faster
    # TODO: what are good defaults here that don't overlap
    def __init__(self, dimensions=1, num_tilings=None, resolutions=None, mem_size=None, safetyval="none"):
        self.dimensions = dimensions
        self.num_tilings = num_tilings or [3, 6]
        self.resolutions = resolutions or [[2], [5]]
        self.mem_size = mem_size or [512]
        self.safetyval = safetyval

        if not safetyval or safetyval == "none":
            self.c_tables = self.mem_size
        else:
            self.c_tables = [CollisionTable(sizeval=m, safetyval=self.safetyval) for m in self.mem_size]

        if len(self.resolutions) != len(self.num_tilings):
            raise Exception("resolutions and num_tilings must be the same length")

        if len(self.mem_size) != len(self.num_tilings):
            raise Exception("mem_size must be the same length as num_tilings")

        for res in resolutions:
            if not isinstance(res, list):
                res = [res]

            if len(res) != dimensions:
                raise Exception("resolution dimensions does not match dimensions")

        self.sparse_size = sum(num_tilings)

        self.buffer = [0] * self.sparse_size
        self.offset_buffer = np.zeros(self.sparse_size, dtype=int)

        offset = 0
        for idx, num in enumerate(num_tilings):
            if idx:
                self.offset_buffer[offset:num] += self.mem_size[idx - 1]

            offset += num

        self.full_size = sum(self.mem_size)

    def tile_it(self, vals, offset=0):
        """
        vals should already be normalized
        """
        # TODO: not thread safe

        buffer_offset = 0
        for idx, num in enumerate(self.num_tilings):
            scaled = (np.array(vals) * np.array(self.resolutions[idx])).tolist()
            loadtiles(tiles=self.buffer,
                      startelement=buffer_offset,
                      numtilings=num,
                      memctable=self.c_tables[idx],
                      floats=scaled)
            buffer_offset += num

            # print("Collisions")
            # for c_table in self.c_tables:
            # print(c_table.collisions)

        return (self.offset_buffer + np.array(self.buffer, dtype=int) + offset).tolist()


class TileCoder:
    '''
        All values should already be normalized from 0 to 1
    '''

    # TODO: take in a buffer
    def __init__(self, dimension=1, num_tilings=4, resolutions=10):
        # num_tilings - the number of tilings to create
        # resolutions - can be a single integer in which case all dimensions have the same resolution. Can be a list o
        #   of resolutions in which case the tilings may have different resolutions along different dimensions

        self.dimension = dimension
        self.num_tilings = num_tilings
        self.resolutions = resolutions
        self.divisions = []
        self.dim_lengths = []
        self.tiling_size = 0
        self.all_tilings_size = 0

        if type(resolutions) is list:
            assert len(resolutions) == dimension

        self._init_divisions()
        self._init_dimensions()

    def _init_divisions(self):
        self.divisions = []
        for idx in range(self.dimension):
            self.divisions.append(1.0 / self._get_resolution(idx))

    def _init_dimensions(self):
        self.tiling_size = 1;
        for idx in range(self.dimension):
            self.dim_lengths.append(self._get_tiling_size_along_dimension(idx))
            self.tiling_size *= self.dim_lengths[idx]

        self.all_tilings_size = self.tiling_size * self.num_tilings

    def _get_resolution(self, idx):
        if type(self.resolutions) is list:
            return self.resolutions[idx]

        return self.resolutions

    def _calculate_tiling_offset(self, dim_idx, tiling_idx):
        # supposedly evenly spaced offsets are a bad idea.
        return tiling_idx * self.divisions[dim_idx] / self.num_tilings

    def _calculate_tiled_index(self, val, dim_idx, tiling_idx):
        # idx - which dimension
        idx = int(float(val + self._calculate_tiling_offset(dim_idx, tiling_idx)) / self.divisions[dim_idx])
        return max(min(self._get_tiling_size_along_dimension(dim_idx) - 1, idx), 0)

    def _get_tiling_size_along_dimension(self, idx):
        return self._get_resolution(idx) + 1;

    def _calculate_tiling_size(self):
        tot = 0;
        for idx in range(self.dimension):
            tot += self._get_tiling_size_along_dimension
        return tot

    def _create_tiling(self, vals, tiling_idx):
        ind = 0;
        pos = 0;
        offset_dimensions = 1
        for idx, val in enumerate(vals):
            div = self.divisions[idx]
            tile_index = self._calculate_tiled_index(val, idx, tiling_idx)
            pos += tile_index * offset_dimensions;
            offset_dimensions *= self._get_tiling_size_along_dimension(idx)

        return pos

    def tile_indices(self, vals):
        assert len(vals) == self.dimension
        # if max(vals) > 1:
        # raise Warning("Val exceeds 1")
        # if min(vals) < 0:
        # raise Warning("Val is less than 0")

        indices = []
        for tile_idx in range(self.num_tilings):
            indices.append(self._create_tiling(vals, tile_idx) + tile_idx * self.tiling_size)

        return indices



