# Continuous Actor Critic as defined in the ICORR 2011 paper - Online Human Training of a Myoelectric Prosthesis Conmtroller via ACRL

from pylab import *
import numpy as np
import random
import math, time
# import os
# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
# import tensorflow as tf

class discrete_ACRL():
    def __init__(self,gamma = 0.99, alphaV = 0.1, alphaU = 0.01, lmbda = 0.7, n=2048, num_actions = 4):
        self.gamma = gamma
        self.alphaV = alphaV
        self.alphaU = alphaU
        self.lmbda = lmbda
        self.action = 0

        self.eu = np.zeros((n,num_actions))
        self.ew = np.zeros(n)

        self.w = np.zeros(n)
        self.u = np.zeros((n,num_actions))

        self.delta = 0.0
        self.value = 0.0
        self.nextValue = 0.0

        self.compatibleFeatures = np.zeros((n,num_actions))
        self.n = n
        self.num_actions = num_actions
        self.gamma_lmbda = gamma * lmbda

    def Value(self,features):
        self.value = np.dot(self.w, features)

    def Next_Value(self,features):
        self.nextValue = np.dot(self.w, features)

    def Calc_Delta(self, reward):
        self.delta = reward + (self.gamma * self.nextValue) - self.value

    def Trace_Update_Critic(self,features):
        self.ew = np.multiply(self.gamma_lmbda, self.ew)
        self.ew += features

    def Trace_Update_Actor(self):
        self.eu = np.multiply(self.gamma_lmbda, self.eu)
        self.eu += self.compatibleFeatures

    def Weights_Update_Critic(self):
        #self.w[:,self.action]  += self.alphaV * self.delta * self.ew[:,self.action]
        self.w += np.multiply(self.alphaV * self.delta, self.ew)

    def Weights_Update_Actor(self):
        self.u += np.multiply(self.alphaU * self.delta, self.eu)

    def Compatible_Features(self,action_prob, features, action_list = None):
        action_list = action_list or np.arange(self.num_actions).tolist()

        self.compatibleFeatures = np.zeros((self.n,self.num_actions))
        self.compatibleFeatures[:,self.action] = features
        for i in action_list:
            self.compatibleFeatures[:,i] -= action_prob[str(i)] * features

    def sample_action(self, action_prob, action_list = None):
        #self.action = int(np.where(action_prob.cumsum() >= np.random.random())[0][0])
        action_list = action_list or np.arange(self.num_actions).tolist()
        action_prob = action_prob.values()
        action = np.random.choice(action_list, p=np.array(action_prob))
        return action

        #return self.action

    def gibbs_action_sampler(self, features, action_list = None):

        #Returns a dictionary of corresponding probabilities and actions

        action_list = action_list or np.arange(self.num_actions).tolist()
        gibbs_num = [np.exp(np.dot(self.u[:,i], features)) for i in action_list]
        gibbs_den = sum(gibbs_num)
        prob = [i / gibbs_den for i in gibbs_num]

        action_prob = {}
        for ind, action in enumerate(action_list):
            action_prob[str(action)] = prob[ind]
        # print action_prob, sum(action_prob.values())
        return action_prob

    def Erase_Traces(self):
        self.eu = np.zeros((self.n, self.num_actions))
        self.ew = np.zeros(self.n)

    def learning_update(self, reward, action_prob, current_features, next_features, action_list = None):
        self.Value(current_features)
        self.Next_Value(next_features)
        self.Calc_Delta(reward)
        self.Trace_Update_Critic(current_features)
        self.Weights_Update_Critic()

        self.Compatible_Features(action_prob, current_features, action_list)
        self.Trace_Update_Actor()
        self.Weights_Update_Actor()

#########################################################################################################################
# Sparse features

class discrete_ACRL_SparseFeatures():   # Assuming its softmax policy
    def __init__(self,gamma = 0.99, alphaV = 0.1, alphaU = 0.01, lmbda = 0.7, n = 2048, num_actions = 4):
        self.gamma = gamma
        self.alphaV = alphaV
        self.alphaU = alphaU
        self.lmbda = lmbda
        self.action = 0
        self.gamma_lmbda = gamma * lmbda

        self.n = n
        self.num_actions = num_actions

        self.w = np.zeros(n)
        self.ew = np.zeros(n)

        self.u = np.zeros((n,num_actions))
        self.eu = np.zeros((n,num_actions))

        self.delta = 0.0
        self.value = 0.0
        self.nextValue = 0.0

        self.compatibleFeatures = np.zeros((n,num_actions))


    # def weight_variable(self, shape):
    #     initial = tf.truncated_normal(shape, stddev=0.1)
    #     return tf.Variable(initial)
    #
    # def trace_variable(self, shape):
    #     initial = tf.zeros(shape)
    #     return tf.Variable(initial)

    def Value(self,features):
        self.value = np.sum(np.take(self.w, features))

    def Next_Value(self,features):
        self.nextValue = np.sum(np.take(self.w, features))

    def Calc_Delta(self, reward):
        self.delta = reward + (self.gamma*self.nextValue) - self.value

    def Trace_Update_Critic(self,features):
        self.ew *= self.gamma_lmbda
        self.ew[features] = 1

    def Trace_Update_Actor(self, features):
        self.eu *= self.gamma_lmbda
        self.eu[features,:] += self.compatibleFeatures[features,:]

    def Weights_Update_Critic(self):
        self.w += self.alphaV * self.delta * self.ew

    def Weights_Update_Actor(self):
        self.u += self.alphaU * self.delta * self.eu

    def Compatible_Features(self, action_prob, features):
        self.compatibleFeatures = np.zeros((self.n,self.num_actions))
        self.compatibleFeatures[features,self.action] = 1

        for i in range(self.num_actions):
            self.compatibleFeatures[features,i] -= action_prob[i]

    def Erase_Traces(self):
        self.ew = np.zeros(self.n)
        self.eu = np.zeros((self.n,self.num_actions))

    def sample_action(self, action_prob):
        return np.random.choice(np.arange(self.num_actions), p=np.array(action_prob))
        #return np.where(action_prob.cumsum() >= np.random.random())[0][0]

    def gibbs_action_sampler(self, features):
        gibbs_num = [np.exp(np.sum(self.u[features,i])) for i in range(self.num_actions)]
        gibbs_den = sum(gibbs_num)
        action_prob = [i/gibbs_den for i in gibbs_num]
        #print action_prob, sum(action_prob)
        return action_prob

    def learning_update(self, reward, action_prob, current_features, next_features, state_action_features = None):
        self.Value(current_features)
        self.Next_Value(next_features)
        self.Calc_Delta(reward)
        self.Trace_Update_Critic(current_features)
        self.Weights_Update_Critic()

        self.Compatible_Features(action_prob, current_features)
        self.Trace_Update_Actor(current_features)
        self.Weights_Update_Actor()




    
