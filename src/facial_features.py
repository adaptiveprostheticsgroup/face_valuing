#!/usr/bin/env python
#from __future__ import print_function
import numpy as np
import math, time
import pickle
import roslib
#roslib.load_manifest('my_package')
import sys
import rospy
import cv2
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import *
from sensor_msgs.msg import Image, CameraInfo
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
from face_valuing.srv import face_check
from face_valuing.msg import faceArray

import matplotlib
matplotlib.use('TkAgg')

from matplotlib import pyplot as plt
import dlib
import Tkinter as tk
import threading
from PIL import Image as PIL_Image
from PIL import ImageTk

LEFT_EYE_POINTS = list(range(42, 48))
RIGHT_EYE_POINTS = list(range(36, 42))
LEFT_BROW_POINTS = list(range(22, 27))
RIGHT_BROW_POINTS = list(range(17, 22))
NOSE_POINTS = list(range(27, 35))
MOUTH_POINTS = list(range(48, 61))
OVERLAY_POINTS = [LEFT_BROW_POINTS + RIGHT_BROW_POINTS + MOUTH_POINTS]
# MOUTH_POINTS = list(range(53, 58))

PREDICTOR_PATH = '/home/gautham/catkin_ws/src/face_valuing/shape_predictor_68_face_landmarks.dat'
NUM_FACIAL_LANDMARKS = 68


## Switch to using white background and black foreground
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')

# win = pg.GraphicsWindow(title="Histogram of images")
# win.resize(1000,600)

class face_valuing:
    def __init__(self, root_window = tk.Tk()):
        self.image_pub = rospy.Publisher("camera_image_rgb",Image, queue_size=10 )
        self.facial_landmarks_pub = rospy.Publisher("facial_features",faceArray, queue_size=10)

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("/camera1/usb_cam1/image_raw",Image,self.callback)
        self.root_window = root_window

        self.face_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml')
        self.min_neighbors = 5
        self.scale_factor = 1.3
        self.face_feat_on = False

        self.detector = dlib.get_frontal_face_detector()
        self.predictor = dlib.shape_predictor(PREDICTOR_PATH)

        self.image_frame = tk.Frame(self.root_window, width=300, height=250)
        self.image_frame.grid(row=0, column=0, sticky='NSEW')
        self.image_label = tk.Label(self.image_frame)
        self.image_label.grid(row=0, column=0, sticky='NSEW')

        self.s = rospy.Service('face_feat_srv', face_check, self.face_detect)


        # plt.ion()
        # plt.figure(figsize=(14, 8), dpi=100, facecolor='w', edgecolor='k')
        self.updateDisplay()

    def face_detect(self, req):
        return self.face_feat_on

    def callback(self,data):
        try:
            self.cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            self.image_pub.publish(self.bridge.cv2_to_imgmsg(self.cv_image, "bgr8"))
        except CvBridgeError as e:
            print(e)

        gray_img = cv2.cvtColor(self.cv_image, cv2.COLOR_BGR2GRAY)
        rects_img = self.detector(gray_img, 2)  # 1 is the upsampling factor used by dlib

        if len(rects_img) == 1:
            facial_landmarks = np.array([[p.x, p.y] for p in self.predictor(gray_img, rects_img[0]).parts()])
            facial_landmarks = self.normalizeFeaturesfromCenter(facial_landmarks)
            facial_landmarks = facial_landmarks[OVERLAY_POINTS]
            self.face_feat_on = True

        elif len(rects_img) > 1:
            print 'Multiple faces detected!'
        elif len(rects_img) == 0:
            print 'No faces detected!'

        h = Header()
        h.stamp = rospy.Time.now()
        h.frame_id = 'Facial_features'
        msg = facial_landmarks.flatten().tolist()
        self.facial_landmarks_pub.publish(faceArray(h, msg))


    def updateDisplay(self):
        frame = cv2.flip(self.cv_image, 1)
        gray_img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = self.face_cascade.detectMultiScale(gray_img, self.scale_factor, self.min_neighbors)
        for (x, y, w, h) in faces:
            frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        cv2_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)



    def normalizeFeaturesfromCenter(self, features):
        features_normalized = features - features[27, :]
        return features_normalized

def main(args):
    root_window = tk.Tk()
    ic = face_valuing(root_window)
    # root_window.attributes('-topmost', True)
    # root_window.after_idle(root_window.attributes, '-topmost', False)
    # root_window.mainloop()

    try:
        # if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        #     QtGui.QApplication.instance().exec_()
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    rospy.init_node('facial_features', anonymous=True)
    main(sys.argv)