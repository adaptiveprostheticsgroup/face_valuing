#!/usr/bin/env python
#from __future__ import print_function
import numpy as np
import math, time
import pickle
import roslib
#roslib.load_manifest('my_package')
import sys
import rospy
import cv2
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import *
from sensor_msgs.msg import Image, CameraInfo
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
from face_valuing.srv import histogram_check
from face_valuing.msg import histArray

## Switch to using white background and black foreground
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')

# win = pg.GraphicsWindow(title="Histogram of images")
# win.resize(1000,600)

class histogram_features:

    def __init__(self):
        self.image_pub = rospy.Publisher("converted_image",Image, queue_size=10 )
        self.hist_pub = rospy.Publisher("histogram_features",histArray, queue_size=10)

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("/camera1/usb_cam1/image_raw",Image,self.callback)
        self.hist_on = False
        # Histogram plot
        self.colors = ('b', 'g', 'r')

        self.s = rospy.Service('hist_feat_srv', histogram_check, self.hist_detect)

        # Plotting Initialization
        # self.plot_window = win.addPlot(title="RGB Histogram of External Camera")
        # self.plot_window.setXRange(0, 256,padding=0)
        # self.curves = []
        # for color in self.colors:
        #     self.curves.append(self.plot_window.plot(pen=color))

    def hist_detect(self, req):
        return self.hist_on

    def callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        (rows,cols,channels) = cv_image.shape
        msg = []
        for i, col in enumerate(self.colors):
            histr = cv2.calcHist([cv_image], [i], None, [20], [0, 256])
            msg += histr.flatten().tolist()
            #self.curves[i].setData(histr.flatten())
            #time.sleep(0.01)

        #cv2.imshow("Image window", cv_image)
        #cv2.waitKey(3)

        self.hist_on = True

        h = Header()
        h.stamp = rospy.Time.now()
        h.frame_id = 'Histogram_features'
        self.hist_pub.publish(histArray(h,msg))

        try:
            self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
        except CvBridgeError as e:
            print(e)

def main(args):
    ic = histogram_features()
    try:
        # if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        #     QtGui.QApplication.instance().exec_()
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    rospy.init_node('image_converter', anonymous=True)
    main(sys.argv)