#!/usr/bin/env python

import random, math
import rospy
from std_msgs.msg import *
from bento_controller.srv import *
from bento_controller.msg import *
import numpy as np, time
from ACRL import *
from feature_builder import *
from face_valuing.srv import face_check, histogram_check
from face_valuing.msg import faceArray, histArray
import cv2, dlib
# ROS initializations
rospy.init_node("bento_grip_selector")
pause_srv = rospy.ServiceProxy("/bento/pause", Pause)
pause_srv.call(False)

def state_callback(data):
    global bento_position, bento_velocity
    position = []
    velocity = []

    for i in range(5):
        position.append(data.joint_states[i].current_pos)
        velocity.append(data.joint_states[i].velocity)

    bento_position = position
    bento_velocity = velocity

def face_callback(msg):
    global face_array
    face_array = msg.faceArray

def hist_callback(msg):
    global hist_array
    hist_array = msg.histArray

pub = rospy.Publisher('/bento/command', BentoCommand, queue_size=10)
sub = rospy.Subscriber("/bento/state", BentoState, state_callback)
#rospy.Subscriber("/facial_features", faceArray, face_callback)
#rospy.Subscriber("/histogram_features", histArray, hist_callback)

# Grip aperture definition
object_image_path = "/home/gautham/catkin_ws/src/face_valuing/"
face_image_path = "/home/gautham/Pictures/Face Valuing/"
grip_apertures = {'orange_cylinder': 1.29, 'fully_open': 0.83, 'neon_cone': 1.45, 'green_obj': 1.90}
object_images = {'orange_cylinder': cv2.imread(object_image_path+"orange_cylinder.jpg"),
                 'neon_cone': cv2.imread(object_image_path+"neon_cone.jpg"),
                 'green_obj': cv2.imread(object_image_path+"green_obj.jpg"),} # egg_cecutor: 1.69

facial_expressions = {'smile': cv2.imread(face_image_path+"Smile #1.jpg"),
                      'frown': cv2.imread(face_image_path+"Frown #1.jpg"),
                      'neutral': cv2.imread(face_image_path+"Neutral #1.jpg"),}

grip_keys = grip_apertures.keys()

#Episode end conditions
elbow_limit = 3.17

# Grip changing station/ Go home command
shoulder_home = 2.6
elbow_home = 3.40

# If a particular grip is chosen
elbow_start = 3.93
shoulder_start = 3.16

# Actions
up = 0.1
down = -0.1

#Face Valuing
LEFT_EYE_POINTS = list(range(42, 48))
RIGHT_EYE_POINTS = list(range(36, 42))
LEFT_BROW_POINTS = list(range(22, 27))
RIGHT_BROW_POINTS = list(range(17, 22))
NOSE_POINTS = list(range(27, 35))
MOUTH_POINTS = list(range(48, 61))
OVERLAY_POINTS = [LEFT_BROW_POINTS + RIGHT_BROW_POINTS + MOUTH_POINTS]
# MOUTH_POINTS = list(range(53, 58))

PREDICTOR_PATH = '/home/gautham/catkin_ws/src/face_valuing/shape_predictor_68_face_landmarks.dat'
NUM_FACIAL_LANDMARKS = 68

# Learning specific initializations
tile_code_2d = TileCoder(dimension=2, num_tilings=10,resolutions=[10,10])
facial_features_size = len(OVERLAY_POINTS) * tile_code_2d.all_tilings_size
position_features_size = 5 #tile_code_2d.all_tilings_size
hist_features_size = 60
n = facial_features_size  + position_features_size + hist_features_size  + 1
m = (len(OVERLAY_POINTS) * tile_code_2d.num_tilings) + 5 + 60 + 1

print m,n
bento = discrete_ACRL(gamma = 1, alphaV = math.pow(10,-3), alphaU = math.pow(10,-4), lmbda = 0.3,n=n,num_actions=3)
#tile_coder = RichTileCoderGroup(dimensions=1, mem_size=mem_size, num_tilings=num_tilings,resolutions=resolutions,safetyval='unsafe')


num_episodes = 500

def pick_a_grip():
    return random.choice(grip_keys)

def do_nothing(t=0.1):
    tic = time.clock()
    toc = time.clock()
    while toc-tic < t:
        toc = time.clock()

def go_to_start():
    # Go to episode start
    print "----------------- Go to start -----------------------"
    pub.publish(header=Header(stamp=rospy.Time().now()),
                joint_commands=[JointCommand(id=1, type="position_and_velocity", position=shoulder_start, velocity=1.5),
                                JointCommand(id=2, type="position_and_velocity", position=elbow_start,velocity=1.5)])
    do_nothing(2)

def go_to_station():
    # Go home, i.e., grip changing station
    pub.publish(header=Header(stamp=rospy.Time().now()),
                joint_commands=[JointCommand(id=1, type="position_and_velocity", position=shoulder_home, velocity=1.5),
                                JointCommand(id=2, type="position_and_velocity", position=elbow_home,velocity=1.5)])
    do_nothing(2)

def scale_facial_features(face_array):
    x_range = [-100, 100]
    y_range = [50, 200]

    face_array = face_array.reshape((-1,2))
    for i in range(face_array.shape[0]):
        face_array[i][0] = (face_array[i][0] - x_range[0])/(x_range[1]-x_range[0])
        face_array[i][1] = (face_array[i][1] - y_range[0])/(y_range[1]-y_range[0])

    face_array = face_array.flatten()
    return face_array.tolist()

def scale_hist_features(hist_array, num_pixels=76800):
    return (hist_array/num_pixels).tolist()

def scale_position(pos):
    x = np.copy(pos)
    return (x/(2 * math.pi)).tolist()

def norm_signal(x, min_x = -2, max_x = 2):
    x = np.clip(x, min_x, max_x)
    return ((x - min_x) / (max_x - min_x)).tolist()


def bento_at_station():
    global bento_position
    if math.fabs(bento_position[1] - elbow_home) < 0.02 and math.fabs(bento_position[0] - shoulder_home) < 0.02:
        return True
    else:
        return False

def bento_at_start():
    global bento_position
    if math.fabs(bento_position[1] - elbow_start) < 0.02 and math.fabs(bento_position[0] - shoulder_start) < 0.02:
        return True
    else:
        return False

def execute_action(bento, action_prob):
    global bento_position, current_grip
    action = bento.action
    if not action:
        if bento_at_station():
            current_grip = pick_a_grip()
            pub.publish(header=Header(stamp=rospy.Time().now()),
                        joint_commands=[JointCommand(id=5, type="position_and_velocity", position=grip_apertures[current_grip], velocity=1)])
            print "Pick a grip"
        else:
            print "Pick a grip"
            return -2 # Reward signal
            # a = random.choice([0.1, -0.1])
            # pub.publish(header=Header(stamp=rospy.Time().now()),
            #             joint_commands=[JointCommand(id=2, type="position_and_velocity", position=bento_position[1] + a, velocity=1)])
            # bento.action = 1 if a==0.1 else 2
            # print "Pick a grip chosen - but go up/down"
    elif action==1:
        if bento_at_station() and not bento_at_start():
            go_to_start()
        pub.publish(header=Header(stamp=rospy.Time().now()),
                    joint_commands=[JointCommand(id=2, type="position_and_velocity", position=bento_position[1] + up, velocity=1)])

        print "Go up"
    else:
        if bento_at_station() and not bento_at_start():
            go_to_start()
        pub.publish(header=Header(stamp=rospy.Time().now()),
                    joint_commands=[JointCommand(id=2, type="position_and_velocity", position=bento_position[1] + down, velocity=1)])
        print "Go down"
    return -1

def normalizeFeaturesfromCenter(features):
    features_normalized = features - features[27, :]
    return features_normalized

def sparse_to_dense(vals, feat_size):
    phi = np.zeros(feat_size)
    phi[vals] = 1
    return phi

def facial_features(cv_image):
    cv_image = cv2.resize(cv_image, (320, 240))

    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(PREDICTOR_PATH)
    gray_img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
    rects_img = detector(gray_img, 2)  # 1 is the upsampling factor used by dlib

    if len(rects_img) == 1:
        facial_landmarks = np.array([[p.x, p.y] for p in predictor(gray_img, rects_img[0]).parts()])
        facial_landmarks = normalizeFeaturesfromCenter(facial_landmarks)
        facial_landmarks = facial_landmarks[OVERLAY_POINTS]

    feat = []
    for i in range(len(OVERLAY_POINTS)):
        offset = tile_code_2d.all_tilings_size * i
        x = norm_signal(facial_landmarks[i,0],-100, 100)
        y = norm_signal(facial_landmarks[i,1], 50, 200)
        feat += (np.array(tile_code_2d.tile_indices([x,y]))+offset).tolist()
    return sparse_to_dense(feat, facial_features_size)

def hist_features(cv_image):
    cv_image = cv2.resize(cv_image, (320, 240))
    colors = ['b','g','r']
    msg = []
    for i, col in enumerate(colors):
        histr = cv2.calcHist([cv_image], [i], None, [20], [0, 256])
        histr /= 320*240
        msg += histr.flatten().tolist()
    return msg

if __name__ == "__main__":
    global bento_position, bento_velocity, hist_array, face_array, current_grip
    rospy.sleep(1)
    rospy.loginfo("velocity based control")

    smile_signals = facial_features(facial_expressions['smile'])
    frown_signals = facial_features(facial_expressions['frown'])

    # # ROS Service to check if Facial features are available
    # rospy.wait_for_service('face_feat_srv')
    # face_detect = rospy.ServiceProxy('face_feat_srv', face_check)
    # resp = face_detect()
    # while not resp.face_check:
    #     resp = face_detect()
    # print "Facial features are available", resp.face_check
    #
    # # ROS Service to check if Histogram features are available
    # rospy.wait_for_service('hist_feat_srv')
    # hist_detect = rospy.ServiceProxy('hist_feat_srv', histogram_check)
    # resp = hist_detect()
    # while not resp.histogram_check:
    #     resp = hist_detect()
    # print "Histogram features are available", resp.histogram_check

    for episode in range(num_episodes):
        go_to_station()
        phi_current = np.zeros(bento.n)
        phi_current[-1] = 1
        phi_next = np.copy(phi_current)
        current_grip = 'orange_cylinder'
        pub.publish(header=Header(stamp=rospy.Time().now()),
                    joint_commands=[JointCommand(id=5, type="position_and_velocity", position=grip_apertures[current_grip],
                                     velocity=1)])

        object = random.choice(object_images.keys())
        correct_grip_aperture = grip_apertures[object]

        position_signals = scale_position(np.array(bento_position))
        face_signals = random.choice([smile_signals, frown_signals])
        hist_signals = hist_features(object_images[object])

        #signals = position_signals + face_signals + hist_signals
        phi_current[0:facial_features_size] = face_signals
        phi_current[facial_features_size:facial_features_size+position_features_size] = position_signals
        phi_current[n-hist_features_size:n] = hist_signals

        bento.Erase_Traces()
        done = False
        count = 0


        while not done:

            if bento_at_station():
                action_list = [0,1]
            else:
                action_list = [1,2]

            action_prob = bento.gibbs_action_sampler(phi_current, action_list)
            bento.action = bento.sample_action(action_prob, action_list)

            #Take action
            reward = execute_action(bento, action_prob)
            if math.fabs(bento_position[1] - elbow_limit) < 0.02:
                go_to_station()
                done = True #False if bento_position[0] - shoulder_home < 0.02 else True
                if current_grip == object:
                    reward = +10
                    print "Success"
                else:
                    reward = -10
                    print "Failure"


            if count > 30:
                done = True
                reward = -50
                "Fucked up big time"

            if current_grip == object:
                face_signals = smile_signals
            else:
                face_signals = frown_signals

            position_signals = scale_position(np.array(bento_position))
            hist_signals = hist_features(object_images[object])

            phi_next[0:facial_features_size] = face_signals
            phi_next[facial_features_size:facial_features_size + position_features_size] = position_signals
            #phi_next[n - hist_features_size:n] = hist_signals

            tic = time.clock()
            bento.learning_update(reward, action_prob, phi_current, phi_next, action_list)
            toc = time.clock()

            do_nothing(0.3)
            phi_current = phi_next
            if rospy.is_shutdown():
                break

            print "Correct grip = ", object, current_grip, action_prob, count
            count += 1

        print  "-------------------------- Episode {} is complete -------------------------- ".format(episode+1), reward
        if rospy.is_shutdown():
            break


    rospy.sleep(0.2)
    rospy.loginfo("Velocity control complete... waiting for command.")
    rospy.spin()
